package com.project.parking.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Inheritance
@NamedQueries({
	@NamedQuery(name = "Voiture.findAll", query = "from Voiture"),
	@NamedQuery(name = "Voiture.findByPlate", query = "select v from Voiture v where v.plaque like :myPlate")
})
//public abstract class Voiture implements Serializable {
public class Voiture implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	protected String plaque;
	protected String couleur;
	protected String marque;
//	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "contractID", referencedColumnName = "id")
	private Contract contract;
	

//	@Transient
	protected boolean empty;
	
//	protected Voiture() {
//		this.empty = true;
//	}
	
	public Voiture() {
		this.empty = true;
	}

//	protected Voiture(String plaque, String couleur, String marque) {
//		this.plaque = plaque;
//		this.couleur = couleur;
//		this.marque = marque;
//		this.empty = true;
//	}
	
	public Voiture(String plaque, String couleur, String marque) {
		this.plaque = plaque;
		this.couleur = couleur;
		this.marque = marque;
		this.empty = true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlaque() {
		return plaque;
	}

	public void setPlaque(String plaque) {
		this.plaque = plaque;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	
	
	protected boolean isEmpty() {
		return empty;
	}

	protected void setEmpty(boolean empty) {
		this.empty = empty;
	}
	

	@Override
	public boolean equals(Object o) {
		Voiture voiture = (Voiture) o;

		return (this.plaque.equals(voiture.getPlaque()) && this.couleur.equals(voiture.getCouleur())
				&& this.marque.equals(voiture.getMarque()));
	}

	public enum Energie {
		ESSENCE("ESSENCE"), ELECTRIQUE("ELECTRIQUE");
		
		private String name;
		
		private Energie(String name) {
			this.name = name;
		}
		
		public String toString() {
			return name;
		}
	};
}

