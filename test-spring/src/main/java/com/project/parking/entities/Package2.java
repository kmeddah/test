package com.project.parking.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Package2 extends Contract {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	public Package2() {
		
	}
	
    public Package2(float prix) {
        super(prix);
    }

    public float getTarif(float nb) {
        return (nb * prix);
    }
}