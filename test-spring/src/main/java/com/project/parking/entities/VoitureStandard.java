package com.project.parking.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "VoitureStandard.findAll", query = "from VoitureStandard"),
	@NamedQuery(name = "VoitureStandard.findByPlate", query = "select v from Voiture v where v.plaque like :myPlate"),
	@NamedQuery(name = "VoitureStandard.findByParking", query = "select v from Voiture v where v.plaque like :myPlate and v.parking.id = :myParking")
})
public class VoitureStandard extends Voiture {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Energie energie;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name="voitureStandard_parkingID")
	private Parking parking;

	public VoitureStandard() {
		super();

	}
	
	public VoitureStandard(String plaque, String couleur, String marque) {
		super(plaque, couleur, marque);
	}

	public VoitureStandard(String plaque, String couleur, String marque, Energie energie) {
		super(plaque, couleur, marque);
		this.energie = energie;
	}
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Energie getEnergie() {
		return energie;
	}

	public void setEnergie(Energie energie) {
		this.energie = energie;
	}

	public Parking getParking() {
		return parking;
	}
	

	public void setParking(Parking parking) {
		this.parking = parking;
	}
	
	@Override
	public boolean isEmpty() {
		return empty;
	}

	@Override
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

}
