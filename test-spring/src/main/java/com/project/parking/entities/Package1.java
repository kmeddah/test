package com.project.parking.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Package1 extends Contract {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private float montant;

	public Package1() {
		
	}
	
	public Package1(float prix, float montant) {
		super(prix);
		this.montant = montant;
	}

	public float getMontant() {
		return montant;
	}

	public void setMontant(float montant) {
		this.montant = montant;
	}

	public float getTarif(float nb) {
		return (montant + nb * prix);
	}
}
