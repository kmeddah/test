package com.project.parking.entities;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.sql.DataSource;

import org.hibernate.annotations.IndexColumn;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@NamedQueries({
	@NamedQuery(name = "Parking.findAll", query = "from Parking"),
	@NamedQuery(name = "Parking.findById", query = "from Parking where id = :myid"),
	@NamedQuery(name = "Parking.findByLocation", query = "from Parking where name = :myName and street = :myStreet and zipCode = :myZipCode and city = :myCity")
})
public class Parking implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("street")
	private String street;
	@JsonProperty("zipCode")
	private String zipCode;
	@JsonProperty("city")
	private String city;
//	@Transient
	@JsonProperty("nbPlacesTotal")
	private int nbPlacesTotal;
	@Transient
	private int nbPlacesTotalVoitureElectrique;
//	@Transient
	@JsonProperty("nbPlacesTotalVoitureElectrique20")
	private int nbPlacesTotalVoitureElectrique20;
//	@Transient
	@JsonProperty("nbPlacesTotalVoitureElectrique50")
	private int nbPlacesTotalVoitureElectrique50;
//	@Transient
	@JsonProperty("nbPlacesTotalVoitureStandard")
	private int nbPlacesTotalVoitureStandard;
	@Column(name = "CurVoitureStandard")
	private int nbCurrentPlacesVoitureStandard;
	@Column(name = "CurVoitureElectrique20")
	private int nbCurrentPlacesVoitureElectrique20;
	@Column(name = "CurVoitureElectrique50")
	private int nbCurrentPlacesVoitureElectrique50;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "parking_voitureElectrique", orphanRemoval = true)
	@IndexColumn(name = "list_index", base = 0)
	private VoitureElectrique voituresElectrique[];

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "parking", orphanRemoval = true)
	@IndexColumn(name = "list_index", base = 0)
	private VoitureStandard voituresStandard[];

	
	public Parking() {
		
	}

	public Parking(int nbPlacesTotal, int nbPlacesTotalVoitureElectrique20, int nbPlacesTotalVoitureElectrique50,
			int nbPlacesTotalVoitureStandard) {
		super();
		this.nbPlacesTotal = nbPlacesTotal;
		this.nbPlacesTotalVoitureElectrique20 = nbPlacesTotalVoitureElectrique20;
		this.nbPlacesTotalVoitureElectrique50 = nbPlacesTotalVoitureElectrique50;
		this.nbPlacesTotalVoitureElectrique = nbPlacesTotalVoitureElectrique20 + nbPlacesTotalVoitureElectrique50;
		this.nbPlacesTotalVoitureStandard = nbPlacesTotalVoitureStandard;
		this.nbCurrentPlacesVoitureStandard = 0;
		this.nbCurrentPlacesVoitureElectrique20 = 0;
		this.nbCurrentPlacesVoitureElectrique50 = 0;
//		voituresElectrique = new ArrayList<VoitureElectrique>(nbPlacesTotalVoitureElectrique);

		voituresStandard = new VoitureStandard[nbPlacesTotalVoitureStandard];
		for (int i = 0; i < voituresStandard.length; i++) {
			voituresStandard[i] = new VoitureStandard();
			voituresStandard[i].setParking(this);
		}
		
		voituresElectrique = new VoitureElectrique[nbPlacesTotalVoitureElectrique];
		for (int i = 0; i < voituresElectrique.length; i++) {
			
			voituresElectrique[i] = new VoitureElectrique();
			if (i < nbPlacesTotalVoitureElectrique20) {
				voituresElectrique[i].setType("20kw");
				
			} else {
				voituresElectrique[i].setType("50kw");
			}
			voituresElectrique[i].setParking(this);
		}

	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getNbPlacesTotal() {
		return nbPlacesTotal;
	}

	public int getNbPlacesTotalVoitureStandard() {
		return nbPlacesTotalVoitureStandard;
	}

	public int getNbPlacesTotalVoitureElectrique20() {
		return nbPlacesTotalVoitureElectrique20;
	}

	public int getNbPlacesTotalVoitureElectrique50() {
		return nbPlacesTotalVoitureElectrique50;
	}

	public int getNbCurrentPlacesVoitureStandard() {
		return nbCurrentPlacesVoitureStandard;
	}

	public void setNbPlacesTotal(int nbPlacesTotal) {
		this.nbPlacesTotal = nbPlacesTotal;
	}

	public void setNbCurrentPlacesVoitureStandard(int nbCurrentPlacesVoitureStandard) {
		this.nbCurrentPlacesVoitureStandard = nbCurrentPlacesVoitureStandard;
	}

	public int getNbCurrentPlacesVoitureElectrique20() {
		return nbCurrentPlacesVoitureElectrique20;
	}

	public void setNbCurrentPlacesVoitureElectrique20(int nbCurrentPlacesVoitureElectrique20) {
		this.nbCurrentPlacesVoitureElectrique20 = nbCurrentPlacesVoitureElectrique20;
	}

	public int getNbCurrentPlacesVoitureElectrique50() {
		return nbCurrentPlacesVoitureElectrique50;
	}

	public void setNbCurrentPlacesVoitureElectrique50(int nbCurrentPlacesVoitureElectrique50) {
		this.nbCurrentPlacesVoitureElectrique50 = nbCurrentPlacesVoitureElectrique50;
	}

	public VoitureStandard[] getVoituresStandard() {
		return voituresStandard;
	}
	

	public VoitureElectrique[] getVoituresElectrique() {
		return voituresElectrique;
	}

	public void setVoituresElectrique(VoitureElectrique[] voituresElectrique) {
		this.voituresElectrique = voituresElectrique;
	}

	public void setVoituresStandard(VoitureStandard voituresStandard[]) {
		this.voituresStandard = voituresStandard;
	}
	

	public synchronized int entree(Voiture v) {

		int place = -1;

		if (v instanceof VoitureElectrique) {
			VoitureElectrique voitureElectrique = (VoitureElectrique) v;
			String type = voitureElectrique.getType();
			
			if (type.equals("20kw")&& nbCurrentPlacesVoitureElectrique20 < nbPlacesTotalVoitureElectrique20) {
				
				place = put(voituresElectrique, "VoitureElectrique", voitureElectrique);
				
			} else if (type.equals("50kw") && nbCurrentPlacesVoitureElectrique50 < nbPlacesTotalVoitureElectrique50) {
				place = put(voituresElectrique, "VoitureElectrique", voitureElectrique);
				
				
			} else {
				System.out.println("Le parkign est ful");
			}

//			if (voituresElectrique.size() < nbPlacesTotalVoitureElectrique) {
//
//				if (type == "20kw" && nbCurrentPlacesVoitureElectrique20 < nbPlacesTotalVoitureElectrique20) {
//					voituresElectrique.add(voitureElectrique);
//
//					nbCurrentPlacesVoitureElectrique20++;
//				} else if (type == "50kw" && nbCurrentPlacesVoitureElectrique50 < nbPlacesTotalVoitureElectrique50) {
//					
//					voituresElectrique.add(voitureElectrique);
//
//					nbCurrentPlacesVoitureElectrique50++;
//					
//				} else {
//					
//			System.out.println("Le parkign est ful");
//				}
//			}

		} else if (v instanceof VoitureStandard) {

			VoitureStandard voitureStandard = (VoitureStandard) v;
			if (nbCurrentPlacesVoitureStandard < nbPlacesTotalVoitureStandard) {

				place = put(voituresStandard, "VoitureStandard", voitureStandard);

			} else {

				System.out.println("Le parkign est ful");
			}

		}

		return place;

	}

	private synchronized int put(Object object[], String objectType, Voiture voiture) {
		int i = 0;
		VoitureElectrique voituresElectrique[] = null;
		VoitureStandard voituresStandard[] = null;

		if (objectType.equals("VoitureStandard")) {
			voituresStandard = (VoitureStandard[]) object;
			VoitureStandard voitureStandard = (VoitureStandard) voiture;
			
			while (i < nbPlacesTotalVoitureStandard) {

				if (voituresStandard[i].isEmpty()) {
					voitureStandard.setEmpty(false);
					voituresStandard[i] = voitureStandard;
					nbCurrentPlacesVoitureStandard++;
					return i;
				}
				i++;
			}

			if (i == nbPlacesTotalVoitureStandard) {
				return -1;
			}

		} else if (objectType.equals("VoitureElectrique")) {
			voituresElectrique = (VoitureElectrique[]) object;
			VoitureElectrique voitureElectrique = (VoitureElectrique) voiture;
			if (voitureElectrique.getType().equals("20kw")) {

				while (i < nbPlacesTotalVoitureElectrique20) {

					if (voituresElectrique[i].isEmpty()) {
						voitureElectrique.setEmpty(false);
						voituresElectrique[i] = voitureElectrique;
						nbCurrentPlacesVoitureElectrique20++;
						return i;
					}
					i++;
				}

				if (i == nbPlacesTotalVoitureElectrique50) {
					return -1;
				}

			} else if (voitureElectrique.getType().equals("50kw")) {
				while (i < nbPlacesTotalVoitureElectrique50) {

					if (voituresElectrique[i].isEmpty()) {
						voitureElectrique.setEmpty(false);
						voituresElectrique[i] = voitureElectrique;
						nbCurrentPlacesVoitureElectrique50++;
						return i;
					}
					i++;
				}

				if (i == nbCurrentPlacesVoitureElectrique50) {
					return -1;
				}

			}
		}

		return -1;

	}

	private synchronized int indexOf(Object object[], String objectType, Voiture voiture) {
		int i = 0;
		VoitureElectrique voituresElectrique[] = null;
		VoitureStandard voituresStandard[] = null;

		if (objectType.equals("VoitureStandard")) {
			voituresStandard = (VoitureStandard[]) object;
			VoitureStandard voitureStandard = (VoitureStandard) voiture;
			
			
			
			while (i < nbCurrentPlacesVoitureStandard) {
				
				if (!voituresStandard[i].isEmpty()) {
					if (voituresStandard[i].equals(voitureStandard)) {

						return i;
					}
				}
				i++;
			}

			if (i == nbPlacesTotalVoitureStandard) {
				return -1;
			}

		} else if (objectType.equals("VoitureElectrique")) {
			voituresElectrique = (VoitureElectrique[]) object;
			VoitureElectrique voitureElectrique = (VoitureElectrique) voiture;
			while (i < nbCurrentPlacesVoitureElectrique20 + nbCurrentPlacesVoitureElectrique50) {
				if (!voituresElectrique[i].isEmpty()) {
					if (voituresElectrique[i].equals(voitureElectrique)) {
						return i;
					}
				}
				i++;
			}

			if (i == nbPlacesTotalVoitureElectrique) {
				return -1;
			}
		}

		return -1;
	}

	public synchronized int remove(Object object[], String objectType, Voiture voiture) {
		int i = 0;
		VoitureElectrique voituresElectrique[] = null;
		VoitureStandard voituresStandard[] = null;

		if (objectType.equals("VoitureStandard")) {
			voituresStandard = (VoitureStandard[]) object;
			VoitureStandard voitureStandard = (VoitureStandard) voiture;
			while (i < nbCurrentPlacesVoitureStandard) {

				if (voituresStandard[i].equals(voitureStandard)) {
					voituresStandard[i].setCouleur(null);
					voituresStandard[i].setMarque(null);
					voituresStandard[i].setPlaque(null);
					voituresStandard[i].setContract(null);
					voituresStandard[i].setEmpty(true);
					nbCurrentPlacesVoitureStandard--;
					return i;
				}
				i++;
			}

			if (i == nbPlacesTotalVoitureStandard) {
				return -1;
			}

		} else if (objectType.equals("VoitureElectrique")) {
			voituresElectrique = (VoitureElectrique[]) object;
			VoitureElectrique voitureElectrique = (VoitureElectrique) voiture;
			
			System.out.println("nbPlacesTotalVoitureElectrique : " + nbPlacesTotalVoitureElectrique);
			
			while (i < voituresElectrique.length) {
				if (!voituresElectrique[i].isEmpty()) {
					if (voituresElectrique[i].equals(voitureElectrique)) {
						voituresElectrique[i].setCouleur(null);
						voituresElectrique[i].setMarque(null);
						voituresElectrique[i].setPlaque(null);
						voituresElectrique[i].setContract(null);
						voituresElectrique[i].setEmpty(true);
						if (voitureElectrique.getType().equals("20kw")) {
							nbCurrentPlacesVoitureElectrique20--;
						} else if (voitureElectrique.getType().equals("50kw")) {
							nbCurrentPlacesVoitureElectrique50--;
						}
						return i;
					}
				}
				i++;
			}

			if (i == nbPlacesTotalVoitureElectrique) {
				return -1;
			}
		}

		return -1;

	}

	public synchronized Facture sortie(Voiture v, Contract c, LocalDateTime dateSortie) {
		int index = 0;
		Facture fact = new Facture();
		String plaque = v.getPlaque();

		if (v instanceof VoitureElectrique) {
//			VoitureElectrique voitureElectrique = (VoitureElectrique) v;
			index = indexOf(voituresElectrique, "VoitureElectrique", v);
			
			
			if (index > -1) {
				fact.setDateSortie(dateSortie.toString());
				fact.setDescription("Facture Voiture (Electrique) " + voituresElectrique[index].getPlaque());
				fact.setPlace(index);
				fact.setTotal(fact.calcTotal(c));
				fact.setVoiture(voituresElectrique[index]);
				remove(voituresElectrique, "VoitureElectrique", v);
			}
			
			
//			
//			fact.setDescription("Facture Voiture Electrique");
//			fact.setVoiture(voituresElectrique.get(index));
//			
//			if (index > -1) {
//				
//				voituresElectrique.remove(index);
//				if (voitureElectrique.getType().equals("20kw")) {
//					nbCurrentPlacesVoitureElectrique20--;
//					
//				} else if (voitureElectrique.getType().equals("50kw")) {
//					
//					nbCurrentPlacesVoitureElectrique50--;
//					
//				}
//			}

		} else if (v instanceof VoitureStandard) {
			index = indexOf(voituresStandard, "VoitureStandard", v);
			
			
			if (index > -1) {
				fact.setDateSortie(dateSortie.toString());
				fact.setDescription("Facture Voiture (Standard) " + voituresStandard[index].getPlaque());
				fact.setPlace(index);
				fact.setTotal(fact.calcTotal(c));
				fact.setVoiture(voituresStandard[index]);
				remove(voituresStandard, "VoitureStandard", v);
			}

		}

		return fact;
	}
	
	public static int getPlace(int voitureParkingid, String voitureType) {
		
		java.sql.Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int res = -1;

		
		try {
			
			String SELECT_QUERY = "";
			if (voitureType.equals("VoitureStandard")) {
				SELECT_QUERY = "SELECT min(id) AS value FROM   voiture WHERE DTYPE = 'VoitureStandard' AND empty = 1 AND voiturestandard_parkingid = ?";
			} else if (voitureType.equals("VoitureElectrique")) {	
				SELECT_QUERY = "SELECT min(id) AS value FROM   voiture WHERE DTYPE = 'VoitureElectrique' AND empty = 1 AND voitureElectrique_parkingID = ?";
			}
			ApplicationContext appContext = (ApplicationContext) new ClassPathXmlApplicationContext("beans.xml");
			DataSource datasource = (DataSource) appContext.getBean("datasource2");
			conn = datasource.getConnection();
			stmt = conn.prepareStatement(SELECT_QUERY);
			stmt.setInt(1, voitureParkingid);
			rs = stmt.executeQuery();
			rs.next();
			res = rs.getInt("value");
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return res;
	}

}
