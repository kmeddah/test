package com.project.parking.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Inheritance
@NamedQueries({
	@NamedQuery(name = "Contract.findAll", query = "from Contract"),
	@NamedQuery(name = "Contract.findById", query = "from Contract where id = :myid")
})
public class Contract implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    protected float prix;
//	@Temporal(TemporalType.DATE)
    protected LocalDateTime dateEntree;

    public Contract() {
    	
    }
    
    protected Contract(float prix) {
        this.prix = prix;
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public LocalDateTime getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(String entree) {
        this.dateEntree = LocalDateTime.now();
    }
}
