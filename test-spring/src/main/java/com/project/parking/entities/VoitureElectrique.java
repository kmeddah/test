package com.project.parking.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "VoitureElectrique.findAll", query = "from VoitureElectrique"),
	@NamedQuery(name = "VoitureElectrique.findByPlate", query = "from VoitureElectrique where plaque = :myPlate"),
	@NamedQuery(name = "VoitureElectrique.findByParking", query = "select v from Voiture v where v.plaque like :myPlate and v.parking.id = :myParking"),
	@NamedQuery(name = "VoitureElectrique.findByType", query = "from VoitureElectrique where type = :myType")
})
public class VoitureElectrique extends Voiture {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Energie energie;
	private String type;
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinColumn(name="voitureElectrique_parkingID")
	private Parking parking_voitureElectrique;
	
	
	public VoitureElectrique() {
		super();
	}

	public VoitureElectrique(String plaque, String couleur, String marque, Energie energie, String type) {
		super(plaque, couleur, marque);
		this.energie = energie;
		this.type = type;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Energie getEnergie() {
		return energie;
	}

	public void setEnergie(Energie energie) {
		this.energie = energie;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

	public Parking getParking() {
		return parking_voitureElectrique;
	}

	public void setParking(Parking parking_voitureElectrique) {
		this.parking_voitureElectrique = parking_voitureElectrique;
	}
	
	
}
