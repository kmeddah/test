package com.project.parking.entities;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
	@NamedQuery(name = "Facture.findAll", query = "from Facture")
})
public class Facture {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
//	@Temporal(TemporalType.DATE)
    private LocalDateTime dateSortie;
    private String description;
//	@OneToOne(cascade = { CascadeType.ALL })
    @OneToOne
	@JoinColumn(name = "voitureID", referencedColumnName = "id")
    private Voiture voiture;
	private int place;
    private float total = 0;

    public Facture() {
    	
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(String sortie) {
        this.dateSortie = LocalDateTime.now();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

	public int getPlace() {
		return place;
	}

	public void setPlace(int place) {
		this.place = place;
	}

	public float calcTotal(Contract c) {
//        long heures = 0;
		long secondes = 0;

        LocalDateTime dateEntree = c.getDateEntree();
        Duration duration = Duration.between(dateEntree, dateSortie);
//        heures = duration.getSeconds() / 3600;
        
        
        
//        System.out.println("Tarif à la minute. Changer variable heures en minutes");
        secondes = duration.getSeconds();
        float minutes = 0;
        if (secondes > 60) {
        	minutes = secondes / 60;
        }
        
        
        
        
        if (c instanceof Package2) {
        	
        	
//        	total = ((Package2) c).getTarif(heures);
        	total = ((Package2) c).getTarif(minutes);

        	
        	
        } else if (c instanceof Package1) {
        	
        	
//        	total = ((Package1) c).getTarif(heures);
        	total = ((Package1) c).getTarif(minutes);
        }
        
        return total;
    }

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}
}
