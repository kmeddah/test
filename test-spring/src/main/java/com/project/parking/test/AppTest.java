package com.project.parking.test;

import java.time.LocalDateTime;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestBody;

import com.project.parking.dao.ObjectDao;
import com.project.parking.dao.ObjectDaoImpl;
import com.project.parking.entities.Contract;
import com.project.parking.entities.Facture;
import com.project.parking.entities.Package2;
import com.project.parking.entities.Parking;
import com.project.parking.entities.Voiture;
import com.project.parking.entities.VoitureElectrique;
import com.project.parking.entities.VoitureStandard;
import com.project.parking.rs.Payload;
import com.project.parking.rs.ParkingServiceController.HttpResponse;
import com.project.parking.utils.HibernateUtils;

public class AppTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		
//		test();
//		test2();
		
//		test3();

	}
	
	private static void test() {
		Parking parking = new Parking();
		parking.setId(1);
		VoitureStandard v = new VoitureStandard("AL-513-FX", "Rouge", "Peugeot");
		Package2 c = new Package2(9);

		Payload p = new Payload();
		p.setParking(parking);
		p.setVoiture(v);
		p.setContract(c);
//		p.setContractType("Package2");

		HttpResponse resp = testEntree(p);
		System.out.println("resp.getStatus() : " + resp.getStatus());
		System.out.println("resp.getBody() : " + resp.getBody());

	}
	
	private static void test2() {
		Parking parking = new Parking();
		parking.setId(1);
		VoitureStandard v = new VoitureStandard();
		v.setPlaque("AL-513-FX");
		Package2 c = new Package2();
		c.setId(1);

		Payload p = new Payload();
		p.setParking(parking);
		p.setVoiture(v);
		p.setContract(c);
//		p.setContractType("Package2");

		HttpResponse resp = testSortie(p);		
		System.out.println("resp.getStatus() : " + resp.getStatus());
		System.out.println("resp.getBody() : " + resp.getBody());

	}
	
	public static void test3() {
		Parking parking = new Parking();
		parking.setId(1);
		Voiture v = new Voiture();
		v.setPlaque("AL-514-FP");
		String vtype = "VoitureElectrique";
		Package2 c = new Package2();
		c.setId(1);
		String ctype = "Package1";

		Payload p = new Payload();
		p.setParking(parking);
		p.setVoiture(v);
		p.setVtype(vtype);
		p.setContract(c);
		p.setCtype(ctype);

		HttpResponse resp = testSortie(p);		
		System.out.println("resp.getStatus() : " + resp.getStatus());
		System.out.println("resp.getBody() : " + resp.getBody());
	}
	
	public static HttpResponse testSortie(@RequestBody Payload p) {
		Session session = HibernateUtils.getSession();
		
		Query query = session.createNamedQuery("Parking.findById");
		query.setParameter("myid", p.getParking().getId());
		Parking parking = (Parking) query.uniqueResult();
		
		HttpResponse resp = new HttpResponse();
		String concat = "";
		if (parking == null) {
			resp.setStatus(500);
			concat = "Le parking spécifié par le paramètre n'existe pas.";
			resp.setBody(concat);
			return resp;
		} else {
			query = session.createNamedQuery("Voiture.findByPlate");
			query.setParameter("myPlate", p.getVoiture().getPlaque());
			
			Voiture v = null;
			if (p.getVtype().equals("VoitureStandard")) {
			 v = (VoitureStandard) query.uniqueResult();
			} else if (p.getVtype().equals("VoitureElectrique")) {
				v = (VoitureElectrique) query.uniqueResult();
				System.out.println("((VoitureElectrique) v).getType(): " + ((VoitureElectrique) v).getType());
			}
			query = session.createNamedQuery("Contract.findById");
			query.setParameter("myid", p.getContract().getId());
			Contract c = (Contract) query.uniqueResult();
			
			if (v == null) {
				concat = "v == null";
				resp.setStatus(500);
				resp.setBody(concat);
				return resp;
			}
			
			if (c == null) {
				concat = "c == null";
				resp.setStatus(500);
				resp.setBody(concat);
				return resp;
			}
			
			Facture facture = parking.sortie(v, c, LocalDateTime.now());
			ObjectDao objecDaoFacture = new ObjectDaoImpl<Facture>(session);
			objecDaoFacture.create(facture);
			ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
			objectDaoParking.update(parking);
			ObjectDao objecDaoContract = new ObjectDaoImpl<Contract>(session);
			objecDaoFacture.delete(c);
			concat = "OK";
			resp.setStatus(200);
			resp.setBody(concat);
			
		}
		
		
		session.close();

		return resp;
	}
	
	public static HttpResponse testEntree(Payload p) {
		Session session = HibernateUtils.getSession();

		Contract c = p.getContract();
		c.setDateEntree(LocalDateTime.now().toString());
		
		Voiture v = null;
		if (p.getVtype().equals("VoitureStandard")) {
		 v = new VoitureStandard();
		} else if (p.getVtype().equals("VoitureElectrique")) {
			v = new VoitureElectrique();
			((VoitureElectrique) v).setType(p.getElecType().toString());
		}
		
		v.setContract(c);
		Query query = session.createNamedQuery("Parking.findById");
		query.setParameter("myid", p.getParking().getId());
		
		Parking parking = (Parking) query.uniqueResult();

		HttpResponse resp = new HttpResponse();
		String concat = "";

		if (parking == null) {
			resp.setStatus(500);
			concat = "Le parking spécifié par le paramètre n'existe pas.";
			resp.setBody(concat);
			return resp;
		} else {

//			v.setParking(parking);
			
			Voiture test = null;
			if (p.getVtype().equals("VoitureStandard")) {
				((VoitureStandard) v).setParking(parking);
				query = session.createNamedQuery("VoitureStandard.findByParking");
			} else if (p.getVtype().equals("VoitureElectrique")) {
				((VoitureElectrique) v).setParking(parking);
				query = session.createNamedQuery("VoitureElectrique.findByParking");
			}
			
//			query = session.createNamedQuery("VoitureStandard.findByParking");
			query.setParameter("myPlate", v.getPlaque());
			query.setParameter("myParking", parking.getId());
//			VoitureStandard test = (VoitureStandard) query.uniqueResult();
			if (p.getVtype().equals("VoitureStandard")) {
				test = (VoitureStandard) query.uniqueResult();
			} else if (p.getVtype().equals("VoitureElectrique")) {
				test = (VoitureElectrique) query.uniqueResult();
			}
			
			if (test == null) {
//				System.out.println("La voiture n'est pas dans le parking");

				int id = Parking.getPlace(parking.getId(), p.getVtype());
				v.setId(id);
				int place = parking.entree(v);

				System.out.println("place : " + place);
				
				ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
				objectDaoParking.update(parking);
								

			}

			session.close();
			concat = "OK";
			resp.setStatus(200);
			resp.setBody(concat);
			return resp;
		}

	}
	
	static class HttpResponse {
		private int status;
		private String body;

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}
	}

}
