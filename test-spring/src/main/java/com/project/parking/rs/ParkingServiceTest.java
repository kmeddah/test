package com.project.parking.rs;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//@RestController
public interface ParkingServiceTest {

//	@RequestMapping(value = "/parking/create", method = RequestMethod.POST)
//	public void testCreate(@RequestParam("name") String name, @RequestParam("street") String street,
//			@RequestParam(value = "zipCode") String zipCode, @RequestParam(value = "city") String city,
//			@RequestParam(value = "nbPlacesTotal") int nbPlacesTotal,
//			@RequestParam(value = "nbPlacesTotalVoitureElectrique20") int nbPlacesTotalVoitureElectrique20,
//			@RequestParam(value = "nbPlacesTotalVoitureElectrique50") int nbPlacesTotalVoitureElectrique50,
//			@RequestParam(value = "nbPlacesTotalVoitureStandard") int nbPlacesTotalVoitureStandard);

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void testUpdate(@RequestParam(value = "nbPlacesTotal") int nbPlacesTotal,
			@RequestParam(value = "nbPlacesTotalVoitureElectrique20") int nbPlacesTotalVoitureElectrique20,
			@RequestParam(value = "nbPlacesTotalVoitureElectrique50") int nbPlacesTotalVoitureElectrique50,
			@RequestParam(value = "nbPlacesTotalVoitureStandard") int nbPlacesTotalVoitureStandard);

	@RequestMapping(value = "/read", method = RequestMethod.GET)
	public void testRead(@RequestParam("name") String name, @RequestParam("street") String street,
			@RequestParam(value = "zipCode") String zipCode, @RequestParam(value = "city") String city);

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public void testDelete(@RequestParam("name") String name, @RequestParam("street") String street,
			@RequestParam(value = "zipCode") String zipCode, @RequestParam(value = "city") String city);

	@RequestMapping(value = "/clean", method = RequestMethod.GET)
	public void clean();

	@RequestMapping(value = "/print", method = RequestMethod.GET)
	public void print();

}
