package com.project.parking.rs;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.parking.dao.ObjectDao;
import com.project.parking.dao.ObjectDaoImpl;
import com.project.parking.entities.Contract;
import com.project.parking.entities.Facture;
import com.project.parking.entities.Package1;
import com.project.parking.entities.Package2;
import com.project.parking.entities.Parking;
import com.project.parking.entities.Voiture;
import com.project.parking.entities.VoitureElectrique;
import com.project.parking.entities.VoitureStandard;
import com.project.parking.utils.HibernateUtils;

@RestController
@RequestMapping(value = "/parking", consumes = "application/json", produces = "application/json")
public class ParkingServiceController {

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public HttpResponse testCreate(@RequestBody Parking p) {
		Session session = HibernateUtils.getSession();

		Parking parking = new Parking(p.getNbPlacesTotal(), p.getNbPlacesTotalVoitureElectrique20(),
				p.getNbPlacesTotalVoitureElectrique50(), p.getNbPlacesTotalVoitureStandard());
		parking.setName(p.getName());
		parking.setStreet(p.getStreet());
		parking.setZipCode(p.getZipCode());
		parking.setCity(p.getCity());
		ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
		objectDaoParking.create(parking);

		session.close();

		HttpResponse resp = new HttpResponse();
		resp.setStatus(200);
		resp.setBody("OK");
		return resp;
	}

	@RequestMapping(value = "/entree", method = RequestMethod.POST)
	public HttpResponse testEntree(@Valid @RequestBody Payload p) {
		Session session = HibernateUtils.getSession();

		HttpResponse resp = new HttpResponse();
		String concat = "";
		String ctype = p.getCtype();
		Contract c = null;
		if (ctype.equals("Package1")) {
			c = new Package1();
			((Package1) c).setMontant(Float.parseFloat(p.getMontant().get()));

		} else if (ctype.equals("Package2")) {

			c = new Package2();

		}
		c.setPrix(p.getContract().getPrix());
		c.setDateEntree(LocalDateTime.now().toString());

		Voiture v = null;
		if (p.getVtype().equals("VoitureStandard")) {
			v = new VoitureStandard();
		} else if (p.getVtype().equals("VoitureElectrique")) {
			v = new VoitureElectrique();
			((VoitureElectrique) v).setType(p.getElecType().get());
		}
		v.setPlaque(p.getVoiture().getPlaque());
		v.setCouleur(p.getVoiture().getCouleur());
		v.setMarque(p.getVoiture().getMarque());
		v.setContract(c);
		Query query = session.createNamedQuery("Parking.findById");
		query.setParameter("myid", p.getParking().getId());
		Parking parking = (Parking) query.uniqueResult();

		if (parking == null) {
			resp.setStatus(500);
			concat = "Le parking spécifié par le paramètre n'existe pas.";
			resp.setBody(concat);
			return resp;
		} else {

			Voiture test = null;
			if (p.getVtype().equals("VoitureStandard")) {
				((VoitureStandard) v).setParking(parking);
				query = session.createNamedQuery("VoitureStandard.findByParking");
			} else if (p.getVtype().equals("VoitureElectrique")) {
				((VoitureElectrique) v).setParking(parking);
				query = session.createNamedQuery("VoitureElectrique.findByParking");
			}

//			query = session.createNamedQuery("VoitureStandard.findByParking");
			query.setParameter("myPlate", v.getPlaque());
			query.setParameter("myParking", parking.getId());
//			VoitureStandard test = (VoitureStandard) query.uniqueResult();

			if (p.getVtype().equals("VoitureStandard")) {
				test = (VoitureStandard) query.uniqueResult();
			} else if (p.getVtype().equals("VoitureElectrique")) {
				test = (VoitureElectrique) query.uniqueResult();
			}

			if (test == null) {
//				System.out.println("La voiture n'est pas présente dans le parking. On peut l'ajouter.");
				int id = Parking.getPlace(parking.getId(), p.getVtype());
				v.setId(id);
				int place = parking.entree(v);

				ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
				objectDaoParking.update(parking);

				concat = "OK";
				resp.setStatus(200);
				resp.setBody(concat);

			}

			session.close();

			return resp;
		}
	}

	@RequestMapping(value = "/sortie", method = RequestMethod.POST)
	public HttpResponse testSortie(@RequestBody Payload p) {
		Session session = HibernateUtils.getSession();

		Query query = session.createNamedQuery("Parking.findById");
		query.setParameter("myid", p.getParking().getId());
		Parking parking = (Parking) query.uniqueResult();

		HttpResponse resp = new HttpResponse();
		String concat = "";
		if (parking == null) {
			resp.setStatus(500);
			concat = "Le parking spécifié par le paramètre n'existe pas.";
			resp.setBody(concat);
			return resp;
		} else {
			query = session.createNamedQuery("Voiture.findByPlate");
			query.setParameter("myPlate", p.getVoiture().getPlaque());

			Voiture v = null;
			if (p.getVtype().equals("VoitureStandard")) {
				v = (VoitureStandard) query.uniqueResult();
			} else if (p.getVtype().equals("VoitureElectrique")) {
				v = (VoitureElectrique) query.uniqueResult();
				System.out.println("((VoitureElectrique) v).getType(): " + ((VoitureElectrique) v).getType());
			}
			query = session.createNamedQuery("Contract.findById");
			query.setParameter("myid", p.getContract().getId());
			Contract c = (Contract) query.uniqueResult();

			if (v == null) {
				concat = "v == null";
				resp.setStatus(500);
				resp.setBody(concat);
				return resp;
			}

			if (c == null) {
				concat = "c == null";
				resp.setStatus(500);
				resp.setBody(concat);
				return resp;
			}

			Facture facture = parking.sortie(v, c, LocalDateTime.now());
			ObjectDao objecDaoFacture = new ObjectDaoImpl<Facture>(session);
			objecDaoFacture.create(facture);
			ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
			objectDaoParking.update(parking);
			ObjectDao objecDaoContract = new ObjectDaoImpl<Contract>(session);
			objecDaoFacture.delete(c);
			concat = "OK";
			resp.setStatus(200);
			resp.setBody(concat);

		}

		session.close();

		return resp;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public HttpResponse testDelete(@RequestBody Parking p) {
		Session session = HibernateUtils.getSession();

		Query query = session.createNamedQuery("Parking.findByLocation");
		query.setParameter("myName", p.getName());
		query.setParameter("myStreet", p.getStreet());
		query.setParameter("myZipCode", p.getZipCode());
		query.setParameter("myCity", p.getCity());
		Parking parking = (Parking) query.uniqueResult();
		ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
		objectDaoParking.delete(parking);

		session.close();

		HttpResponse resp = new HttpResponse();
		resp.setBody("OK");
		resp.setStatus(200);
		return resp;
	}

	@RequestMapping(value = "/clean", method = RequestMethod.GET)
	public HttpResponse clean() {
		Session session = HibernateUtils.getSession();

		Query query = session.createNamedQuery("Parking.findAll");
		List<Parking> parkingList = (List<Parking>) query.list();
		ObjectDao objectDaoParking = new ObjectDaoImpl<Parking>(session);
		for (Parking p : parkingList) {
			objectDaoParking.delete(p);
		}

		session.close();

		HttpResponse resp = new HttpResponse();
		resp.setBody("OK");
		resp.setStatus(200);
		return resp;
	}
	
//	@ResponseBody
	@RequestMapping(value = "/print", method = RequestMethod.GET, produces = "application/json")
	public List<Parking> print() {
		Session session = HibernateUtils.getSession();

		GsonBuilder gsonBuilder = new GsonBuilder();

		Query query = session.createNamedQuery("Parking.findAll");
		List<Parking> parkingList = (List<Parking>) query.list();
		
		session.close();
		
		return parkingList;
	}

//	@RequestMapping(value = "/print", method = RequestMethod.GET)
//	public HttpResponse print() {
//		Session session = HibernateUtils.getSession();
//
//		GsonBuilder gsonBuilder = new GsonBuilder();
//
//		Query query = session.createNamedQuery("Parking.findAll");
//		List<Parking> parkingList = (List<Parking>) query.list();
//		Gson gson = gsonBuilder.create();
//		String JSONObject = gson.toJson(parkingList);
//		
//		session.close();
//
//		HttpResponse resp = new HttpResponse();
//		resp.setBody(JSONObject);
//		resp.setStatus(200);
//		return resp;
//	}

	@RequestMapping(value = "/voituresElectrique", method = RequestMethod.GET)
	public VoitureElectrique[] getVoituresElectrique(@RequestBody Parking p) {
		Session session = HibernateUtils.getSession();

		Query query = session.createNamedQuery("Parking.findByLocation");
		query.setParameter("myName", p.getName());
		query.setParameter("myStreet", p.getStreet());
		query.setParameter("myZipCode", p.getZipCode());
		query.setParameter("myCity", p.getCity());
		Parking parking = (Parking) query.uniqueResult();
		VoitureElectrique[] voitureList = parking.getVoituresElectrique();

		session.close();

		return voitureList;
	}

	@RequestMapping(value = "/voituresStandard", method = RequestMethod.GET)
	public VoitureStandard[] getVoituresStandard(@RequestBody Parking p) {
		Session session = HibernateUtils.getSession();

		Query query = session.createNamedQuery("Parking.findByLocation");
		query.setParameter("myName", p.getName());
		query.setParameter("myStreet", p.getStreet());
		query.setParameter("myZipCode", p.getZipCode());
		query.setParameter("myCity", p.getCity());
		Parking parking = (Parking) query.uniqueResult();
		VoitureStandard[] voitureList = parking.getVoituresStandard();

		session.close();

		return voitureList;
	}

	public class HttpResponse {
		private int status;
		private String body;

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}
	}
}
