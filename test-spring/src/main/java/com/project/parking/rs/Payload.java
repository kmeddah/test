package com.project.parking.rs;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.project.parking.entities.Contract;
import com.project.parking.entities.Parking;
import com.project.parking.entities.Voiture;

public class Payload {
	@JsonProperty("parking")
	private Parking parking;
	@JsonProperty("voiture")
	private Voiture voiture;
	@JsonProperty("vtype")
	private String vtype;
	@JsonProperty("elecType")
	private Optional<String> elecType;
	@JsonProperty("contract")
	private Contract contract;
	@JsonProperty("ctype")
	private String ctype;
	@JsonProperty("montant")
	private Optional<String> montant;

	public Parking getParking() {
		return parking;
	}

	public void setParking(Parking parking) {
		this.parking = parking;
	}

	public Voiture getVoiture() {
		return voiture;
	}

	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}
	
	public String getVtype() {
		return vtype;
	}

	public void setVtype(String vtype) {
		this.vtype = vtype;
	}
	
	

	public Optional<String> getElecType() {
		return elecType;
	}

	public void setElecType(String elecType) {
		this.elecType = Optional.of(elecType);
	}

	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public String getCtype() {
		return ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}

	public Optional<String> getMontant() {
		return montant;
	}

	public void setMontant(String montant) {
		this.montant = Optional.of(montant);
	}
	
	
}
