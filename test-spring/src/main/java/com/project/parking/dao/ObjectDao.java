package com.project.parking.dao;

public interface ObjectDao <T> {

	public void create(T t);
	
	public void read(T t);
	
	public void update(T t);
	
	public void delete(T t);
	
	public void clean(Class<?> c);
	
	public void print(Class<?> c);
}
