package com.project.parking.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.project.parking.entities.Facture;
import com.project.parking.entities.Package1;
import com.project.parking.entities.Package2;
import com.project.parking.entities.Parking;
import com.project.parking.entities.VoitureElectrique;
import com.project.parking.entities.VoitureStandard;

public class ObjectDaoImpl<T> implements ObjectDao {
	private Session session;
	private Transaction tx;
	private Query query = null;

	public ObjectDaoImpl(Session session) {
		this.session = session;
	}

	public void create(Object t) {
		// TODO Auto-generated method stub
		tx = session.beginTransaction();

		if (t instanceof Package1) {
			Package1 pack1 = (Package1) t;
//			session.persist(pack1);
			session.save(pack1);
			session.flush();
		} else if (t instanceof Package2) {
			Package2 pack2 = (Package2) t;
//			session.persist(pack2);
			session.save(pack2);
			session.flush();
		} else if (t instanceof VoitureElectrique) {
			VoitureElectrique voitureElectrique = (VoitureElectrique) t;
//			session.persist(voitureElectrique);
			session.save(voitureElectrique);
			session.flush();
		} else if (t instanceof VoitureStandard) {
			System.out.println("Entre vOITUre standard");
			VoitureStandard voitureStandard = (VoitureStandard) t;
//			session.persist(voitureStandard);
			session.save(voitureStandard);
			session.flush();
		} else if (t instanceof Facture) {
			Facture facture = (Facture) t;
//			session.persist(facture);
			session.save(facture);
			session.flush();
		} else if (t instanceof Parking) {
			Parking parking = (Parking) t;
//			session.persist(parking);
			session.save(parking);
			session.flush();
		}
		tx.commit();
	}

	public void read(Object t) {
		// TODO Auto-generated method stub
		tx = session.beginTransaction();

		if (t instanceof Package1) {
			Package1 pack1 = (Package1) t;
			query = session.createNamedQuery("Package1.findById");
			query.setParameter("myid", pack1.getId());
			Package1 _pack1 = (Package1) query.uniqueResult();
			System.out.println("Package1 : [id] = " + _pack1.getId());
			System.out.println("Package1 : [montant] = " + _pack1.getMontant());
			System.out.println("Package1 : [prix] = " + _pack1.getPrix());
		} else if (t instanceof Package2) {
			Package2 pack2 = (Package2) t;
			query = session.createNamedQuery("Package2.findById");
			query.setParameter("myid", pack2.getId());
			Package2 _pack2 = (Package2) query.uniqueResult();
			System.out.println("Package2 : [id] = " + _pack2.getId());
			System.out.println("Package2 : [prix] = " + _pack2.getPrix());
		} else if (t instanceof VoitureElectrique) {
			VoitureElectrique voitureElectrique = (VoitureElectrique) t;
			query = session.createNamedQuery("VoitureElectrique.findByType");
			query.setParameter("myType", voitureElectrique.getType());
			VoitureElectrique _voitureElectrique = (VoitureElectrique) query.uniqueResult();
//			System.out.println("VoitureElectrique : [id] = " + _voitureElectrique.getId());
			System.out.println("VoitureElectrique : [plaque] = " + _voitureElectrique.getPlaque());
			System.out.println("VoitureElectrique : [couleur] = " + _voitureElectrique.getCouleur());
			System.out.println("VoitureElectrique : [marque] = " + _voitureElectrique.getMarque());
			System.out.println("VoitureElectrique : [type] = " + _voitureElectrique.getType());
		} else if (t instanceof VoitureStandard) {
			VoitureStandard voitureStandard = (VoitureStandard) t;
			query = session.createNamedQuery("VoitureStandard.findByType");
			query.setParameter("myEnergie", voitureStandard.getEnergie().toString());
			VoitureStandard _voitureStandard = (VoitureStandard) query.uniqueResult();
//			System.out.println("VoitureStandard : [id] = " + _voitureStandard.getId());
			System.out.println("VoitureStandard : [plaque] = " + _voitureStandard.getPlaque());
			System.out.println("VoitureStandard : [couleur] = " + _voitureStandard.getCouleur());
			System.out.println("VoitureStandard : [marque] = " + _voitureStandard.getMarque());
			System.out.println("VoitureStandard : [energie] = " + _voitureStandard.getEnergie().toString());
		} else if (t instanceof Facture) {
			Facture facture = (Facture) t;
			query = session.createNamedQuery("Facture.findByPlate");
			query.setParameter("myPlaque", facture.getVoiture().getPlaque());
			Facture _facture = (Facture) query.uniqueResult();
			System.out.println("Facture : [id] = " + _facture.getId());
			System.out.println("Facture : [dateSortie] = " + _facture.getDateSortie());
			System.out.println("Facture : [description] = " + _facture.getDescription());
			System.out.println("Facture : [place] = " + _facture.getPlace());
			System.out.println("Facture : [prixTotal] = " + _facture.getTotal());
		} else if (t instanceof Parking) {
			Parking parking = (Parking) t;
			query = session.createNamedQuery("Parking.findById");
			query.setParameter("myid", parking.getId());
			Parking _parking = (Parking) query.uniqueResult();
			System.out.println("Parking : [id] = " + _parking.getId());
			System.out.println("Parking : [name] = " + _parking.getName());
			System.out.println("Parking : [street] = " + _parking.getStreet());
			System.out.println("Parking : [zipCode] = " + _parking.getZipCode());
			System.out.println("Parking : [city] = " + _parking.getCity());
		}
		tx.commit();
	}
	

	public void update(Object t) {
		// TODO Auto-generated method stub
		tx = session.beginTransaction();

		if (t instanceof Package1) {
			Package1 pack1 = (Package1) t;

		} else if (t instanceof Package2) {
			Package2 pack2 = (Package2) t;

		} else if (t instanceof VoitureElectrique) {
			VoitureElectrique voitureElectrique = (VoitureElectrique) t;

		} else if (t instanceof VoitureStandard) {
			VoitureStandard voitureStandard = (VoitureStandard) t;
			session.update(voitureStandard);

		} else if (t instanceof Facture) {
			Facture facture = (Facture) t;

		} else if (t instanceof Parking) {
			Parking parking = (Parking) t;
			session.clear();
			session.update(parking);
		}
		tx.commit();
	}

	public void delete(Object t) {
		tx = session.beginTransaction();

		// TODO Auto-generated method stub
		if (t instanceof Package1) {
			Package1 pack1 = (Package1) t;
			session.delete(pack1);
		} else if (t instanceof Package2) {
			Package2 pack2 = (Package2) t;
			session.delete(pack2);
		} else if (t instanceof VoitureStandard) {
			VoitureStandard voitureStandard = (VoitureStandard) t;
			session.delete(voitureStandard);
		} else if (t instanceof VoitureElectrique) {
			VoitureElectrique voitureElectrique = (VoitureElectrique) t;
			session.delete(voitureElectrique);
		} else if (t instanceof Facture) {
			Facture facture = (Facture) t;
			session.delete(facture);
		} else if (t instanceof Parking) {
			Parking parking = (Parking) t;
			session.delete(parking);
		}
		tx.commit();
	}

	public void clean(Class c) {
		// TODO Auto-generated method stub
		
	}

	public void print(Class c) {
		// TODO Auto-generated method stub
		
	}
}
